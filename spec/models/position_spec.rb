require 'rails_helper'

RSpec.describe Position, type: :model do
  describe 'creation' do
    it 'can be created' do
      topic = Topic.create(name: 'Salud')
      question = Question.create(title: "¿Cuál es su opinión de la salud?")
      position = Position.create(title: 'Salud pública', question: question)
      expect(position).to be_valid
    end
    it "can't be created two positions with the same question and scale" do
      topic = Topic.create(name: 'Salud')
      question = Question.create(title: "¿Cuál es su opinión de la salud?")
      position1 = Position.create(title: 'Salud pública', question: question, scale: 1)
      position2 = Position.create(title: 'Salud privada', question: question, scale: 1)
      expect(position1).to be_valid
      expect(position2).to_not be_valid
    end
    it "can be created two positions with the same question and differents scale" do
      topic = Topic.create(name: 'Salud')
      question = Question.create(title: "¿Cuál es su opinión de la salud?", topic: question)
      position1 = Position.create(title: 'Salud pública', question: question, scale: 1)
      position2 = Position.create(title: 'Salud privada', question: question, scale: 2)
      expect(position1).to be_valid
      expect(position2).to be_valid
    end
    it "can be created two positions with the same scale and differen question" do
      topic = Topic.create(name: 'Salud')
      question1 = Question.create(title: "¿Cuál es su opinión de la salud?", topic: topic)
      question2 = Question.create(title: "¿Qué piensa de la salud?", topic: topic)
      position1 = Position.create(title: 'Salud pública', question: question1, scale: 1)
      position2 = Position.create(title: 'Salud privada', question: question2, scale: 1)
      expect(position1).to be_valid
      expect(position2).to be_valid
    end
  end
end
