require 'rails_helper'

RSpec.describe Actor, type: :model do
  describe 'creation' do
    it 'can be created' do
      actor_category = ActorCategory.create(name: 'Partido')
      actor = Actor.create(name: 'Actor nombre', actor_category: actor_category, description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.')
      expect(actor).to be_valid
    end
  end
end
