require 'rails_helper'

RSpec.describe ActorCategory, type: :model do
  describe 'creation' do
    it 'can be created' do
      actor_category = ActorCategory.create(name: 'Partido político')
      expect(actor_category).to be_valid
    end
  end
end
