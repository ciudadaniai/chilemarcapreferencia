require 'rails_helper'

RSpec.describe Question, type: :model do
  describe 'creation' do
    it 'can be created' do
      topic = Topic.create(name: 'Salud')
      question = Question.create(title: '¿Qué opina de la salud?', topic: topic)
      expect(question).to be_valid
    end
    it "can't be created without topic" do
      # TODO
    end
    it "can't be created two questions with the same title" do
      topic = Topic.create(name: "Educación")
      question1 = Question.create(title: '¿Qué opina de la salud?', topic: topic)
      question2 = Question.create(title: '¿qué opina de la salud?', topic: topic)
      expect(question2).to_not be_valid
    end
  end
end
