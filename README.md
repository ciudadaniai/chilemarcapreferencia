# README

Chile Marca Preferencia es un proyecto de FCI que tiene como objetivo entregar a lxs ciudadanxs datos actualizados respecto de las posturas y definiciones políticas de determinadxs actorxs.

Es una aplicación desarrollada con el framework Ruby on Rails, desarrollada para Chile. Con conocimiento en Rails se puede adaptar para cualquier país.

Se debe clonar la app con <br />
`git clone https://gitlab.com/ciudadaniai/chilemarcapreferencia.git`

En un terminal ir al directorio con <br />
`cd chilemarcaprefererencia`

Para instalar una versión local de la app es necesario: <br />

- Ruby 2.6.6 <br />
Se recomienda manejar las versiones de Ruby con rbenv. La forma básica sería <br />
`rbenv install 2.6.6`

- Postgresql -dependerá de la plataforma o sistema operativo- <br />

- Instalar gemas con <br />
`bundle install`

- Ejecutar <br />
`bundle exec rails webpacker:install`

- Se puede generar un error de yarn, resolver con <br />
`yarn install --check-files`
