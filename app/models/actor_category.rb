class ActorCategory < ApplicationRecord
  has_many :actors

  validates :name, presence: true
end
