class Actor < ApplicationRecord

  belongs_to :actor_category
  has_many :actor_positions
  has_many :positions, through: :actor_positions

  accepts_nested_attributes_for :actor_positions, allow_destroy: true

  scope :partidos, -> { where(actor_category: ActorCategory.find_by_name('Partidos políticos'))}
  scope :movimientos_sociales, -> { where(actor_category: ActorCategory.find_by_name('Movimientos sociales'))}
  scope :sociedad_civil, -> { where(actor_category: ActorCategory.find_by_name('Organizaciones de la sociedad civil'))}

  has_rich_text :content
  has_one_attached :logo

  validates :name, presence: true

  def description_by_actor_position(actor_position_id)
    !actor_position_id.blank? ? ActorPosition.find(actor_position_id).description : ''
  end

  def description_for_position(question)
    self.actor_positions.each do |actor_position|
      if actor_position.question == question
        return actor_position.description
      end
    end
    return ''
  end

end
