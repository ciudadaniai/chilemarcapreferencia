module PreferenceHelper

  def class_by_category(actor_category)
    if actor_category == ActorCategory.find_by_name('Partidos políticos')
      'class-red'
    elsif actor_category == ActorCategory.find_by_name('Movimientos sociales')
      'class-purple'
    elsif actor_category == ActorCategory.find_by_name('Organizaciones de la sociedad civil')
      'class-blue'
    else
      'class-default'
    end
  end

end
