class PreferencesController < ApplicationController
  before_action :set_preference, only: [:show]
  load_and_authorize_resource

  # GET /preferences/1
  def show
  end

  # step1: choose your preference
  def choose_topic
    @topics = Topic.pluck(:name, :id)
  end

  # step2: answer questiones
  def answer_questions
    if !params["topic"]["id"].blank?
      @topic = Topic.includes(questions: :positions).find(params["topic"]["id"])
    else
      redirect_to preferences_choose_topic_path, notice: 'Debes seleccionar un área'
    end
  end

  # Create preference
  # GET /actors/new
  def new
  end

  # POST /actors
  def create
    if !params["preference"]["position_id"].blank?
      @preference = Preference.new(position: Position.find(params["preference"]["position_id"].to_i))
      if @preference.save
        redirect_to preferences_your_preferences_path(id: @preference.id)
      else
        redirect_to preferences_choose_topic_path, notice: @preference.errors
      end
    else
      redirect_to preferences_choose_topic_path, notice: 'Hubo un error, vuelve a intentarlo'
    end
  end

  # step3: answer questiones
  def your_preferences
    request.variant = determine_variant
    @actors =Actor.all
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_preference
      @preference = Preference.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def preference_params
      params.require(:preference).permit(:position_id)
    end

    def determine_variant
      variant = :desktop
      # some code to determine the variant(s) to use
      variant = :mobile if browser.device.mobile?
      variant
    end
end
