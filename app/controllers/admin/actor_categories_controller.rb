class Admin::ActorCategoriesController < Admin::AdminController
  before_action :set_actor_category, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource

  def index
    @actor_categories = ActorCategory.all
  end

  def show
  end

  def new
    @actor_category = ActorCategory.new
  end

  def edit
  end

  def create
    @actor_category = ActorCategory.new(actor_category_params)

    if @actor_category.save
      redirect_to admin_actor_category_url(@actor_category), notice: 'Actor category was successfully created.'
    else
      render :new
    end
  end

  def update
    if @actor_category.update(actor_category_params)
      redirect_to admin_actor_category_url(@actor_category), notice: 'Actor category was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @actor_category.destroy
    redirect_to admin_actor_categories_url, notice: 'Actor category was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_actor_category
      @actor_category = ActorCategory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def actor_category_params
      params.require(:actor_category).permit(:name, :icon)
    end
end
