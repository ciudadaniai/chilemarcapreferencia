class AddPositionAndActorToActorPosition < ActiveRecord::Migration[6.0]
  def change
    add_reference :actor_positions, :actor, null: false, foreign_key: true
    add_reference :actor_positions, :position, null: false, foreign_key: true
  end
end
