class RemoveTopictoPositions < ActiveRecord::Migration[6.0]
  def change
    remove_reference :positions, :topic
  end
end
