class AddTopicToQuestions < ActiveRecord::Migration[6.0]
  def change
    add_reference :questions, :topic
  end
end
