class AddTopicToPositions < ActiveRecord::Migration[6.0]
  def change
    add_reference :positions, :topic
  end
end
